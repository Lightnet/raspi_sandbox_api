/*
    Project Name: Node Web Sandbox API
    Link:https://bitbucket.org/Lightnet/nodewebsandboxapi
    Created By: Lightnet
    License: Please read the readme.txt file for more information.
  
    Information:
    
*/
var clients = [];
var modulelist = [];
var io;
//module.exports.modules = modules = modulelist;
var AddModuleList = function (_module) {
    modulelist.push(_module);
};
module.exports.AddModule = addModule = AddModuleList;
var RemoveModuleList = function (_module) {
    for (var i = 0; i < modulelist.length; i++) {
        if (modulelist[i] == _module) {
        }
    }
};
module.exports.RemoveModule = removeModule = RemoveModuleList;
var getModuleList = function () {
    return modulelist;
};
module.exports.getModules = getModules = getModuleList;
var addAppView = function (_app, _view) {
    var views = _app.get('views');
    views.push(_view);
    _app.set('views', views);
};
module.exports.getModules = addView = addAppView;
