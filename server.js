//sudo node /usr/share/adafruit/webide/repositories/raspi_sandbox_api/nodeserver.js

var http = require('http');
//var IP = "127.0.0.1";
var IP = "0.0.0.0";
var PORT = 3000;

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');  
}).listen(PORT, IP);

console.log('Server running at http://' + IP + ':' +PORT + '/');