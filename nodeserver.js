//https://dzone.com/articles/execute-unix-command-nodejs
var sys = require('sys')
var exec = require('child_process').exec;
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

function puts(error, stdout, stderr) { sys.puts(stdout) };

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
    console.log(msg);
    if(msg == "shutdown"){
        // or more concisely
        exec("sudo shutdown", puts);
    }
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});